import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Login from "./views/Login.vue"
import Trivia from "./views/Trivia.vue"
import Results from "./views/Results.vue"


const routes: RouteRecordRaw[] = [
    {
        path:  "/",
        component : Login
    },
    {
        path: "/trivia",
        component : Trivia
    },
    {
        path: "/results",
        component : Results
    }
];

export default createRouter({
	history: createWebHistory(),
	routes,
});
