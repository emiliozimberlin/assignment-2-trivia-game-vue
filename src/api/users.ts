import { BASE_URL_USERS } from "./index";
import axios from "axios";
const apiKey = "csgoheipaadeg6462";
// Interface for serializing user-object
export interface User {
	username: string;
	highScore: number;
}
// Check if user already exists in DB
export async function userExists(username: string) {
    try {
        const response = await axios.get(`${BASE_URL_USERS}?username=${username}`)

        console.log("userMatch", response.data)
        if (!response.data.length ) {
            console.log("returning false")
            return false;
        }
        return true;

    } catch (error) {
        console.log(error)
    }
}
// Gets a spesific user by username
export async function getUser(
	username: string
): Promise<[string | null, User[]]> {
	try {
		const response = await axios.get(`${BASE_URL_USERS}?username=${username}`);
		console.log(response.data);
		return [null, response.data];
	} catch (error: any) {
		return [error.message, []];
	}
}
// Gets all users from DB
export async function getAllUsers(): Promise<[string | null, User[]]> {
	try {
		const { data } = await axios.get(BASE_URL_USERS);
		console.log("api =" + data);
		return [null, data];
	} catch (error: any) {
		return [error.message, []];
	}
}
// Updates users in DB with a new High score
export async function updateUsers(username: string, highScore: number) {
	try {
		const config = {
			method: "PATCH",
			headers: {
				"X-API-Key": apiKey,
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				username: username,
				highScore: highScore,
			}),
		};
		const response = await fetch(BASE_URL_USERS, config);
		console.log(response);
		return [null, response];
	} catch (error: any) {
		return [error.message, null];
	}
}
// Registers new user in DB
export async function apiUserRegister(username: string) {
	try {
		const config = {
			method: "POST",
			headers: {
				"X-API-Key": apiKey,
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				username: username,
				highScore: 0,
			}),
		};
		const response = await fetch(BASE_URL_USERS, config);
		console.log(response);
		return [null, response];
	} catch (error: any) {
		return [error.message, null];
	}
}
