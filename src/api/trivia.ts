import { BASE_URL_TRIVIA } from "./index";
import axios from "axios";

export interface TriviaResponse {
	responseCode: number;
	results: Trivia[];
}

export interface Trivia {
	category: string;
	type: string;
	difficulty: string;
	question: string;
	correct_answer: string;
	incorrect_answers: string;
}
//Take in trivia objects from the api and serialize them using the above interfaces
export async function getTrivia(): Promise<[string | null, Trivia[]]> {
	try {
		const { data } = await axios.get<TriviaResponse>(BASE_URL_TRIVIA);
		return [null, data.results]
	} catch (error: any) {
		return [error.message, []];
	}
}
