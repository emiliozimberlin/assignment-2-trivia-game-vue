import { InjectionKey } from "vue";
import { createStore, useStore as baseUseStore, Store } from "vuex";
import { Trivia } from "./api/trivia";
import { User } from "./api/users";

export interface State {
	trivia: Trivia[];
    user: User[];
}

export const key: InjectionKey<Store<State>> = Symbol();

export default createStore<State>({
    state:{
        trivia: [],
        user: []
    },
   
    mutations:{
        setTrivia: (state: State, payload: Trivia[]) => {
            state.trivia = [...payload];
            console.log("payload =" + payload)
        },
        setUsers: (state: State, payload: User[]) => {
            state.user =  payload;
            console.log("payload =" + payload)
        },
        
   
    }, 
    getters: {

    },
});

export function useStore() {
	return baseUseStore(key);
}
