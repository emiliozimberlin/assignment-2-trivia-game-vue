# The Trivia Game
A SPA vue TypeScript Trivia game. 
## Table of Contents
- [Description](#Description)
- [Installation](#Installation)
- [Components](#Components)
- [Maintainer](#Maintainer)
- [Author](#Author)
- [License](#License)
## Description
This is a single page application vue TypeScript trivia game
To start add a username and press lets play and take the trivia quiz.
At the and you will see all the users.
The data for the triva queststion and users is provided to with RESTful API.
## Installation
Install node.js.
Clone the repo. Open with Visual studio code and the type "npm install" in the terminal.
Then type "npm run dev" in the terminal

## Known Bugs
The app crashes when creating a new user and going to the results page, fixed on refresh

## Components
This project contains componets, api, views and assests.
Also using store.ts and router.ts

## Maintainer
[@emiliozimberlin](https://gitlab.com/emiliozimberlin) and [@Msh-zaar] (https://gitlab.com/Msh-zaar)

## Author
Emilio Zimberlin and Michael Zaar
## License
[MIT](https://choosealicense.com/licenses/mit/)
